package com.openclassrooms.medilabo.note.IT.controller;

import com.openclassrooms.medilabo.note.model.Note;
import com.openclassrooms.medilabo.note.repository.NoteRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class PostNoteIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private NoteRepository noteRepository;

    @AfterEach
    void tearDown() {
        noteRepository.deleteAll();
    }

    private MockHttpServletRequestBuilder getRequest(Long patientId) {
        return post("/patients/" + patientId + "/notes")
                .contentType(MediaType.APPLICATION_JSON);
    }

    @Test
    @WithMockUser(username = "user1", password = "password1")
    void shouldCreateNoteWhenAuthenticated() throws Exception {
        // Given
        // When
        mockMvc.perform(getRequest(1L)
                        .content("Voici une nouvelle note pour le patient."))
                .andExpect(status().isOk())
                .andReturn();

        // Then
        List<Note> actualNotes = noteRepository.findByPatientId(1L);
        Note expectedNote = Note.builder()
                .patientId(1L)
                .note("Voici une nouvelle note pour le patient.")
                .build();
        assertThat(actualNotes)
                .usingRecursiveFieldByFieldElementComparatorIgnoringFields("id")
                .contains(expectedNote);
    }

    @Test
    void shouldReturn401WhenUnAuthenticated() throws Exception {
        // Given
        // When
        mockMvc.perform(getRequest(1L)
                        .content(""))
                .andExpect(status().isUnauthorized());
        // Then
    }
}

