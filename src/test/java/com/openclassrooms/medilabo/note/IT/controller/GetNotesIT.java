package com.openclassrooms.medilabo.note.IT.controller;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GetNotesIT {

    @Autowired
    private MockMvc mockMvc;

    private MockHttpServletRequestBuilder getRequest(Long patientId) {
        return get("/patients/" + patientId + "/notes")
                .contentType(MediaType.APPLICATION_JSON);
    }

    @Test
    @WithMockUser(username = "user1", password = "password1")
    void shouldReturnNotes() throws Exception {
        // Given
        // When
        MvcResult mvcResult = mockMvc.perform(getRequest(3L))
                .andExpect(status().isOk())
                .andReturn();

        // Then
        String expectedJson = """
                [
                  "Le patient déclare qu'il fume depuis peu.",
                  "Le patient déclare qu'il est fumeur et qu'il a cessé de fumer l'année dernière. Il se plaint également de crises d’apnée respiratoire anormales. Tests de laboratoire indiquant un taux de cholestérol LDL élevé."
                ]""";
        JSONAssert.assertEquals(expectedJson, mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), true);
    }

    @Test
    void shouldReturn401WhenUnAuthenticated() throws Exception {
        // Given
        // When
        mockMvc.perform(getRequest(1L))
                .andExpect(status().isUnauthorized());

        // Then
    }
}
