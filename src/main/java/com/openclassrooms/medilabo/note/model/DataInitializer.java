package com.openclassrooms.medilabo.note.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.medilabo.note.repository.NoteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DataInitializer implements ApplicationRunner {

    private final NoteRepository noteRepository;
    private final ApplicationContext applicationContext;
    private final ObjectMapper objectMapper;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (noteRepository.count() == 0) {
            // Utilisation de try-with-resources pour assurer la fermeture du stream
            try (InputStream inputStream = applicationContext.getResource("classpath:data.json").getInputStream();
                 Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
                List<Note> notes = objectMapper.readValue(reader, new TypeReference<List<Note>>() {
                });
                noteRepository.saveAll(notes);
            }
        }
    }
}
