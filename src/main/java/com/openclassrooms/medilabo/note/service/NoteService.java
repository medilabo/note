package com.openclassrooms.medilabo.note.service;

import com.openclassrooms.medilabo.note.model.Note;
import com.openclassrooms.medilabo.note.repository.NoteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class NoteService {

    private final NoteRepository noteRepository;

    public List<String> getNotes(Long id) {
        List<Note> notes = noteRepository.findByPatientId(id);
        return notes.stream().map(Note::getNote).toList();
    }

    public Note createNote(Long patientId, String note) {
        Note newNote = new Note();
        newNote.setPatientId(patientId);
        newNote.setNote(note);
        return noteRepository.save(newNote);
    }
}
