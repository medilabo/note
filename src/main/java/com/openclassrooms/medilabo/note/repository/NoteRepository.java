package com.openclassrooms.medilabo.note.repository;

import com.openclassrooms.medilabo.note.model.Note;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRepository extends MongoRepository<Note, String>{
    List<Note> findByPatientId(Long id);
}
