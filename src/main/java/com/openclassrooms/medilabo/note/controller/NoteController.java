package com.openclassrooms.medilabo.note.controller;

import com.openclassrooms.medilabo.note.service.NoteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/patients")
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Note")
public class NoteController {

    private final NoteService noteService;

    @GetMapping("/{patientId}/notes")
    @Operation(operationId = "getNotes")
    public ResponseEntity<List<String>> getNotes(
            @PathVariable Long patientId
    ) {
        List<String> notes = noteService.getNotes(patientId);
        return ResponseEntity.ok(notes);
    }

    @PostMapping("/{patientId}/notes")
    @Operation(operationId = "createNote")
    public ResponseEntity<Void> createNote(
            @PathVariable Long patientId,
            @RequestBody String note
    ) {
        noteService.createNote(patientId, note);
        return ResponseEntity.ok().build();
    }
}
